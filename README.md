## Car Management API

# Halaman ke Open API Documentation

http://localhost:8000/api-docs/

# Endpoint

Index Page
http://localhost:8000/

Sign Up (Post)
http://localhost:8000/auth/signup

Sign In (Post)
http://localhost:8000/auth/signin

Current User (Get)
http://localhost:8000/auth/profile

Create New Car (Post)
http://localhost:8000/api/cars

Find All Cars (Get)
http://localhost:8000/cars

Find Car By ID (Get)
http://localhost:8000/cars/:id

Update Car (Put)
http://localhost:8000/cars/:id

Delete Car (Delete)
http://localhost:8000/cars/:id

# Email & Password Admin
Email : lawa@gmail.com
password : 654321
